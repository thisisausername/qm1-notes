# Quantum mechanics 1 lecture notes

Lecture notes and teaching material used for the Delft University of Technology course TN2304.

The compiled materials are available at https://qm1.quantumtinkerer.tudelft.nl

# Origin and technical support

This repository is based on a template for publishing lecture notes, developed
by Anton Akhmerov, who also hosts such repositories for other courses.
